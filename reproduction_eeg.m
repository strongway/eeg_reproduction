function reproduction_eeg
% This experiment is to examine the regression effect observed in block
% reproduction task. Acerbi et al. (2012) paradigm is adopted here. The main
% goal is to uncover eeg signature of 'prior'
%
% coded by Strongway (shi@lmu.de)
% date: 25th Oct., 2014
% version: 0.1
% Revision: 30th Jan., 2015
% What's new:
%   1. 250 ms after the reproduction, providing a feedback display
%   2. replace '^' and 'reproduction' with a dot, removing some distractors
%   3. Adjust production-reproduction interval to 500 ms
%   4. adjust start of trial with a fixation cross of 500 ms
%   5. fixed a small error in sendtrigger function
%   6. Add a swtich for debugging with/without trigger

% revision: 10.02.2015
%   1. add practice trials prior to each block

% revision: add deBruijn sequence (17.02.2015)

% experiment related parameters
para.viewDistance = 57; % viewing distance 57 cm
para.monitor = 22; % monitor size
para.fntSize = 18; % font size
para.bkColor = 128; % background color
para.fColor = 192; % foreground color
para.fColorW = 252; % white color
para.green = [0, 192, 0];
para.red = [192, 0, 0];
para.yPosition = 0; % 0 degree above
para.iti = [1.5, 2]; % range of inter-trial interval
para.iprp = 0.5; % inter production-reproduction interval
para.xyFeedbackArray = [-2,0; -1, 0; 0, 0; 1, 0; 2,  0]; % location of feedback array
para.fbRange = [-100, -0.3; -0.3, -0.05; -0.05, 0.05; 0.05, 0.3; 0.3, 100]; %feedback range with respect to the reproduction error
para.withFeedback = true;
para.vSize = 3; % 3 degree
para.nDurations = [0.5:0.1:0.9; 0.7:0.1:1.1]; % short (0.5-0.9), long (0.7-1.1)

[blkFactors, nDurs] = size(para.nDurations);
% --- change 40 to 50 ---
nTrlsBlk = 50; % number of trials per block 10x5
% ----
inBlkRep = nTrlsBlk/size(para.nDurations,2); %inblock repetition
blkRep = 4; % block repetitions

bUseDeBruijn = false; %flag of using de Bruijn sequence or not
try

    % create experimental design
    exp = CExp(inBlkRep,nDurs,'blockFactors',blkFactors, ...
        'blockRepetition',blkRep);

    % --- add the option of using de Bruijn
    if bUseDeBruijn
        % modify the inblock sequence
        for ib = 1: blkFactors * blkRep
            deseq = debruijn_generator(5,2); % 5 durations, inter-trial combination
            deseq = [deseq; deseq];
            blk_idx = (ib-1)* nTrlsBlk + 1 : (ib-1)*nTrlsBlk + nTrlsBlk;
            exp.seq(blk_idx) = deseq(1:length(blk_idx));
        end
    end
    % acquire subject information
    exp.subInfo;

    % intitialize display, input /output devices

    v = CDisplay('bgColor',para.bkColor,'fontSize',para.fntSize,'monitorSize',para.monitor,...
        'viewDistance',para.viewDistance);
    kb = CInput('k',[1],{'downArrow'});

    % output trigger via parallel port
    % initiate port
    withTrigger = 0;  % only for debugging purpose in Mac
    if withTrigger  % with EEG trigger
        dioOut=digitalio('parallel','LPT1');
        addline(dioOut,0:7,'out');
        putvalue(dioOut,0);
    else
        dioOut = [];
    end

    % create stimuli
    para.vObj = v.createShape('circle',para.vSize,para.vSize,'color',para.fColor);
    para.vObjW = v.createShape('circle',para.vSize,para.vSize,'color',para.fColorW);

    % create feedback stimuli
    vGreenDisk = v.createShape('circle', para.vSize/5, para.vSize/5, 'color',para.green);
    vRedDisk = v.createShape('circle', para.vSize/5, para.vSize/5, 'color',para.red);
    vDiskFrame = v.createShape('circle',para.vSize/5, para.vSize/5, 'color',para.fColorW,'fill',0);
    para.vFullFrames = [vDiskFrame, vDiskFrame, vDiskFrame, vDiskFrame, vDiskFrame];
    para.vFullDisks = [vRedDisk, vGreenDisk, vGreenDisk, vGreenDisk, vRedDisk];
    %initialize text
    infoText = init_text;

    % start instruction
    v.dispText(infoText.instruction);
    kb.wait;
    WaitSecs(2);

    for iTrl=1:exp.maxTrls

        % display block info
        if  mod(iTrl,nTrlsBlk) == 1
            % _____________________
            %  practice trials
            %  newly added by Strongway, 10.02.2015
            %______________________

            v.dispText(infoText.practice);
            kb.wait;
            WaitSecs(0.5);
            pracDurations = [0.7, 0.9, 1.1]; % practice durations
            for i= randperm(length(pracDurations))
                curDuration = pracDurations(i);
                cond = [0 0];
                trialPresentation(v,kb,dioOut, cond, curDuration, para);
            end
            v.dispText(infoText.startBlock);
            kb.wait;
            WaitSecs(0.5);
        end

        %get current condition
        cond = exp.getCondition;  % duration, range
        curDuration = para.nDurations(cond(2),cond(1)); % current standard

        %start trial presentation
        results = trialPresentation(v,kb,dioOut, cond, curDuration, para);

        %store results
        exp.setResp(results);

        % debugging
        if kb.wantStop
            break;
        end

        % ITI 1.5-2 seconds
        WaitSecs(para.iti(1) + (para.iti(2)-para.iti(1))*rand);

    end
    exp.saveData;   %save data
    v.dispText(infoText.thankyou);
    kb.wait;
    v.close;

  catch ME
    % debugging
  	v.close;

    disp(ME.message);
    disp(ME.stack);
    for i=1:length(ME.stack)
        disp(ME.stack(i).name);
        disp(ME.stack(i).line);
    end
    v.close;

end
end

function results = trialPresentation(v,kb,dioOut, cond, curDuration, para)
% trial presentation
% inputs: v.display, kb.keyboard, dio.parallelport, condition, duration,
% other parameters
% output: [curDuration, phyDuration, proDuration, repVDuration, repDuration];

        v.dispFixation(40);

        sendtrigger(dioOut,1); %  eeg trigger, trial start (fixation) - 1

        WaitSecs(0.500); %at least 500 ms

%        v.dispText(infoText.production);
        % standard duration
        % initiated by key pressing, and measured by key release
        v.dispFixation(5,2); % change fixation from cross to a dot

        [key, keyInitTime] = kb.response;
        v.dispItems([0, para.yPosition], para.vObj,[para.vSize para.vSize],0,0); % draw texture
        [vbl, vInitTime] = v.flip;

        sendtrigger(dioOut,cond(2)*10+cond(1)); %eeg trigger for duration conditions: 11, 12, ..., 15, 21, ..., 25

        WaitSecs(curDuration - 0.009); % 9 ms earlier, so make sure to clear next frame on time.

        % show a visual marker (a white disk), prompting release key
        %v.dispItems([2 2], vObjW,[2 2],0,0);
        [vbl, vStopTime] = v.flip(1);

        sendtrigger(dioOut,30); % end of standard duration

        keyReleaseTime = kb.keyRelease;
        sendtrigger(dioOut,31); % end of key release
        v.flip(1); %clear screen

        phyDuration = vStopTime - vInitTime;        %visual duration
        proDuration = keyReleaseTime - keyInitTime; %key production

        % reproduction
        WaitSecs(para.iprp); %wait at least 250 ms
%        v.dispText(infoText.reproduction);
        v.dispFixation(5,2);


        [key, keyInitTime] = kb.response;
        v.dispItems([0, para.yPosition], para.vObj,[para.vSize para.vSize],0,0);
        [vbl, vInitTime] = v.flip;

        sendtrigger(dioOut,50); % start of reproduction

        keyReleaseTime = kb.keyRelease;

        sendtrigger(dioOut,51); % end of reproduction

        [vbl, vStopTime]=v.flip(1);

        repDuration = keyReleaseTime - keyInitTime; % key reproduction
        repVDuration = vStopTime - vInitTime; % visual reproduction

        %store results
        results = [curDuration, phyDuration, proDuration, repVDuration, repDuration];

        if para.withFeedback
            % present a feedback display
            feedbackDisplay = para.vFullFrames;
            delta = (repDuration - phyDuration)/phyDuration;
            % find the range of the error
            cIdx = para.fbRange > delta; % column index of left and right boundary
            idx = find(xor(cIdx(:,1),cIdx(:,2)));
            feedbackDisplay(idx(1)) = para.vFullDisks(idx(1));

            WaitSecs(0.25); % wait 250 ms
            v.dispItems(para.xyFeedbackArray, feedbackDisplay,[para.vSize/5 para.vSize/5]); % draw texture
            WaitSecs(0.500); % display the feedback for 500 ms
            v.flip;
        end

end
function infoText = init_text
    % specify experimental text
    infoText.instruction = [ 'Instruction \n\n',...
        'On each trial, you will be asked to start the trial by pressing the key.', ...
        'Immediately after your key pressing, you will see a circle appears, and it will disappear automatically after a short time. ', ...
        'You are asked to release the key immediately. After a short blank, your will be asked to reproduce that duration ', ...
        ' by pressing the key as long as the previous presented duration \n'];
    infoText.practice = 'Practice \n Press a key to start';
    infoText.formalInfo = 'The formal experiment will start soon, \n please press a key to start\n';
    infoText.blockInfo = 'Please take a rest, and when you are ready, please press a key to start. \n ';
    infoText.endTrial = 'Please release the key';
    infoText.production = '+';
    infoText.reproduction = '+';
     infoText.startBlock = ' Please press any key to start this block';
     infoText.goingon = 'Please press any key to go on this block';
      %  infoText.feedback = ['The actual duration is ', num2str(x), ' ms'];

     infoText.thankyou = 'The experiment is finished! \nThank you very much!';

end

function sendtrigger(dioOut, value)
    if isempty(dioOut)
        return;
    end
    % send trigger for 2 ms. 
    putvalue(dioOut,value);
    WaitSecs(0.002);  
    putvalue(dioOut,0);
end